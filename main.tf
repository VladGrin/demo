provider "aws" {
  access_key = "AKIA5MTNET5ZXMN2PK7E"
  secret_key = "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"
  region = "eu-west-3"
}

resource "aws_instance" "linux_instance" {
  ami = "ami-05b93cd5a1b552734"
  instance_type = "t2.micro"
  key_name = "vlad-key-Paris"

  vpc_security_group_ids = [
    aws_security_group.linux_instance.id]

  tags = {
    Name = "Linux instance"
  }
}

resource "aws_eip" "linux_instance" {
  connection {
    type = "ssh"
    user = "ec2-user"
    host = "${aws_instance.linux_instance.public_ip}"
    private_key = "${file("vlad-key-Paris.pem")}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum install docker -y whatever",
      "sudo service docker start",
      "sudo docker pull vladgrin/demo-1-repo:v.1",
      "sudo docker run -d -p 80:777 vladgrin/demo-1-repo:v.1"
    ]
  }
}

resource "aws_security_group" "linux_instance" {
  name = "myCustomSecurityGroup"
  description = "bla-bla"

  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    from_port = 443
    protocol = "tcp"
    to_port = 443
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}