FROM openjdk:8
COPY ./target/demo.jar /usr/app/
WORKDIR /usr/app
RUN sh -c 'touch demo.jar'
EXPOSE 777
CMD ["java", "-jar", "demo.jar"]